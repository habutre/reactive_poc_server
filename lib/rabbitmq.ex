defmodule ReactivePocServer.RabbitMQ do
  @doc "Function which retrieves the latest (top) message from queue"
  @callback retrieve_message() :: %ReactivePocServer.RabbitMQ.Message{}


  @doc "Function which publish a message on queue"
  @callback publish_message(message :: ReactivePocServer.RabbitMQ.Message) :: %ReactivePocServer.RabbitMQ.Message{}
end
