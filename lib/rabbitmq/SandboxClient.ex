defmodule ReactivePocServer.RabbitMQ.SandboxClient do
  @behaviour ReactivePocServer.RabbitMQ
  @exchange "reactive_poc_exchange"

  alias ReactivePocServer.RabbitMQ.Message

  def retrieve_message() do
    %Message{}
  end

  def publish_message(message) do
    nil
  end

  defp publisher(message) do
    connection_params = [host: "192.168.99.101", port: 30100, virtual_host: "/", username: "rabbitmq", password: "s3cr3t"]
    {:ok, connection} = AMQP.Connection.open(connection_params)
    {:ok, channel}    = AMQP.Channel.open(connection)
    {:ok, exchange}   = AMQP.Exchange.declare(channel, @exchange)
    {:ok, message}    = AMQP.Basic.publish(channel, @exchange, "", message)
    IO.puts message
    :ok
  end
end
