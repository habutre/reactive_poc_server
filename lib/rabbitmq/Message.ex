defmodule ReactivePocServer.RabbitMQ.Message do
  defstruct [:id, :created_at, :type, :body]
end
