defmodule ReactivePocServer do
  @moduledoc """
  Documentation for ReactivePocServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ReactivePocServer.hello
      :world

  """
  def hello do
    :world
  end

  def random_message(message) do
    :ok
  end
end
