defmodule ReactivePocServer.Messenger do

  def random_message() do
    :fail
  end

  def random_message(message) do
    connection_params = [host: "192.168.99.101", port: 30100, virtual_host: "/", username: "rabbitmq", password: "s3cr3t"]
    {:ok, connection} = AMQP.Connection.open(connection_params)
    {:ok, channel} = AMQP.Channel.open(connection)
    {:ok, queue} = AMQP.Queue.declare(channel, "reactive_poc")
    {:ok, message} = AMQP.Basic.publish(channel, "", "reactive_poc", message)
    IO.puts message
    :ok
  end
end
