defmodule ReactivePocServerTest.Messenger do
  use ExUnit.Case
  doctest ReactivePocServer.Messenger

  test "Given an available messenger \
        When a nil message is sent to messenger publishs \
        Then the messenger returns :fail" do

    assert ReactivePocServer.Messenger.random_message() == :fail
  end

  test "Given an available messenger \
        When a new message is sent to messenger publishs \
        Then the messenger returns :ok" do
    assert ReactivePocServer.random_message("expect any message here...") == :ok
  end
end
